use crate::components::backend::appinstance;
use crate::components::backend::appinstance::ContextType;
use crate::components::ui::elements::common as commonui;
use crate::components::ui::elements::{help_page, main_page};
use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use std::io;
use tui::{
    backend::{Backend, CrosstermBackend},
    Frame, Terminal,
};

pub fn setup(app: appinstance::AppInstance) -> Result<(), io::Error> {
    // setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // Run the ui
    run_ui(&mut terminal, app)?;

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    Ok(())
}

pub fn run_ui<B: Backend>(
    terminal: &mut Terminal<B>,
    mut app: appinstance::AppInstance,
) -> io::Result<()> {
    loop {
        terminal.draw(|f| ui(f, &mut app))?;

        // Perform action on key
        if let Event::Key(key) = event::read()? {
            match app.get_current_context_type() {
                ContextType::MainPage => {
                    main_page::register_key_event(key, &mut app);
                }
                ContextType::HelpPage => {
                    help_page::register_key_event(key, &mut app);
                }
                _ => return Ok(()),
            };
        }
        if let ContextType::Quit = app.get_current_context_type() {
            return Ok(());
        }
    }
}

pub fn ui<B: Backend>(f: &mut Frame<B>, app: &mut appinstance::AppInstance) {
    /*
     * Ensure that the terminal area is large enough for the application
     * This part has to be run before the rest of the rendering
     * If not, it will allow ui elements to render even if there is not enough room!
     */

    if f.size().area() < 700 {
        commonui::draw_size_too_small_widget(f);
        return;
    }

    match app.get_current_context_type() {
        ContextType::MainPage => {
            main_page::draw(f, app, f.size());
        }
        ContextType::HelpPage => {
            help_page::draw(f, app, f.size());
        }
        _ => {}
    };
    return;
}
