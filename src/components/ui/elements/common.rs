use crate::components::backend::appinstance;
use crate::components::backend::appinstance::ContextType;
use tui::{
    backend::Backend,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, BorderType, Borders, Clear, List, ListItem, Paragraph, Wrap},
    Frame,
};

pub fn draw_list<B: Backend>(
    f: &mut Frame<B>,
    app: &mut appinstance::AppInstance,
    area: Rect,
    title: &str,
) {
    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .margin(1)
        .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
        .split(area);
    let items: Vec<ListItem> = app
        .master_list
        .list()
        .iter()
        .map(|i| {
            let lines = vec![Spans::from(i.name.to_string())];
            ListItem::new(lines).style(Style::default().fg(Color::Black).bg(Color::White))
        })
        .collect();
    let items = List::new(items)
        .block(Block::default().borders(Borders::ALL).title(title))
        .highlight_style(
            Style::default()
                .bg(Color::LightGreen)
                .add_modifier(Modifier::BOLD),
        )
        .highlight_symbol(">> ");

    f.render_stateful_widget(items, chunks[0], &mut app.master_list.state);
    if app.master_list.state.selected().is_some() {
        // Draw text
        let selected = app.master_list.state.selected();
        let text = vec![
            Spans::from(
                String::from("Name: ") + app.master_list.list()[selected.unwrap()].name.as_str(),
            ),
            Spans::from(
                String::from("Priority: ")
                    + app.master_list.list()[selected.unwrap()]
                        .priority
                        .to_string()
                        .as_str(),
            ),
            Spans::from(
                String::from("Catagory: ")
                    + app.master_list.list()[selected.unwrap()]
                        .catagory
                        .to_string()
                        .as_str(),
            ),
            Spans::from(selected.unwrap().to_string()),
        ];
        let block = Block::default().borders(Borders::ALL).title("Task Info");
        let paragraph = Paragraph::new(text).block(block).wrap(Wrap { trim: true });
        f.render_widget(paragraph, chunks[1]);
    }
}

pub fn draw_popup<B: Backend>(f: &mut Frame<B>, percent_x: u16, percent_y: u16) {
    // Render popup box
    let block = Block::default()
        .borders(Borders::ALL)
        .title(Span::styled(
            "Popup",
            Style::default()
                .bg(Color::Magenta)
                .add_modifier(Modifier::BOLD),
        ))
        .title_alignment(Alignment::Center)
        .border_type(BorderType::Rounded);
    let popup_area = centered_rect(percent_x, percent_y, f.size());
    f.render_widget(Clear, popup_area);
    f.render_widget(block, popup_area);
}

pub fn draw_debug_popup<B: Backend>(
    f: &mut Frame<B>,
    app: &mut appinstance::AppInstance,
    area: Rect,
) {
    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .margin(1)
        .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
        .split(area);
    // Render popup box
    let block = Block::default()
        .borders(Borders::ALL)
        .title(Span::styled(
            "DEBUG",
            Style::default().add_modifier(Modifier::BOLD),
        ))
        .title_alignment(Alignment::Center)
        .border_type(BorderType::Rounded);

    let text = vec![
        Spans::from(
            "AppInstance CurrentContext: ".to_string() + app.current_context.to_string().as_str(),
        ),
        if let ContextType::MainPage = app.get_current_context_type() {
            Spans::from(
                "MainPage NewPopUpState: ".to_string()
                    + app.main_context().new_proj_popup.to_string().as_str(),
            )
        } else {
            Spans::from("")
        },
    ];
    let paragraph = Paragraph::new(text)
        .style(Style::default().fg(Color::Red))
        .block(block)
        .wrap(Wrap { trim: false });
    let layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(70), Constraint::Percentage(30)].as_ref())
        .split(chunks[1]);
    let popup_area = off_center_rect(40, 40, layout[1], 1, 1);
    f.render_widget(Clear, popup_area);
    f.render_widget(paragraph, popup_area);
}

pub fn draw_size_too_small_widget<B: Backend>(f: &mut Frame<B>) {
    let block = Block::default()
        .borders(Borders::NONE)
        .title(Span::styled(
            "Terminal size too small",
            Style::default()
                .fg(Color::White)
                .bg(Color::Red)
                .add_modifier(Modifier::BOLD),
        ))
        .title_alignment(Alignment::Center);
    f.render_widget(block, f.size());
}

// Helper function for calculating popup size
fn centered_rect(percent_x: u16, percent_y: u16, r: Rect) -> Rect {
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage((100 - percent_y) / 2),
                Constraint::Percentage(percent_y),
                Constraint::Percentage((100 - percent_y) / 2),
            ]
            .as_ref(),
        )
        .split(r);

    Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage((100 - percent_x) / 2),
                Constraint::Percentage(percent_x),
                Constraint::Percentage((100 - percent_x) / 2),
            ]
            .as_ref(),
        )
        .split(popup_layout[1])[1]
}

// Helper function for calculating popup size
fn off_center_rect(
    percent_x: u16,
    percent_y: u16,
    r: Rect,
    mut vert_factor: u16,
    mut hori_factor: u16,
) -> Rect {
    if vert_factor == 0 {
        vert_factor = 1;
    }
    if hori_factor == 0 {
        hori_factor = 1;
    }
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage((100 - percent_y) / vert_factor),
                Constraint::Percentage(percent_y),
                Constraint::Percentage((100 - percent_y) / vert_factor),
            ]
            .as_ref(),
        )
        .split(r);

    Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage((100 - percent_x) / hori_factor),
                Constraint::Percentage(percent_x),
                Constraint::Percentage((100 - percent_x) / hori_factor),
            ]
            .as_ref(),
        )
        .split(popup_layout[1])[1]
}
