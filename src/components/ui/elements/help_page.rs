use crate::components::backend::appinstance::AppInstance;
use crate::components::backend::appinstance::ContextType;
use crate::components::ui::elements::common;
use crossterm::event::{KeyCode, KeyEvent};
use tui::{
    backend::Backend,
    layout::Rect,
    text::Spans,
    widgets::{Block, Borders, Paragraph, Wrap},
    Frame,
};

pub struct HelpContext {
    debug_popup: bool,
}

impl HelpContext {
    pub fn new() -> HelpContext {
        HelpContext { debug_popup: false }
    }

    pub fn debug<'a>(&'a mut self, dbg: bool) -> &'a mut HelpContext {
        self.debug_popup = dbg;
        self
    }

    pub fn create(&self) -> HelpContext {
        HelpContext {
            debug_popup: self.debug_popup,
        }
    }
}
pub fn draw<B: Backend>(f: &mut Frame<B>, app: &mut AppInstance, area: Rect) {
    let text = vec![
        Spans::from("Controls"),
        Spans::from("   n: New project"),
        Spans::from("   ?: Help page"),
        Spans::from("   q: Quit"),
    ];
    let block = Block::default().borders(Borders::ALL).title("Help");
    let paragraph = Paragraph::new(text).block(block).wrap(Wrap { trim: true });
    f.render_widget(paragraph, area);
    if app.help_context.debug_popup == true {
        common::draw_debug_popup(f, app, area);
    }
}

pub fn register_key_event(key: KeyEvent, app: &mut AppInstance) {
    match key.code {
        KeyCode::Char('q') | KeyCode::Char('?') | KeyCode::Esc => {
            app.set_current_context_type(ContextType::MainPage);
        }
        KeyCode::Left => app.master_list.unselect(),
        KeyCode::Down => app.master_list.next(),
        KeyCode::Up => app.master_list.previous(),
        _ => {}
    }
}
