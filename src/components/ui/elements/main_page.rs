use crate::components::backend::appinstance::AppInstance;
use crate::components::backend::appinstance::ContextType;
use crate::components::ui::elements::common;
use crossterm::event::{KeyCode, KeyEvent};
use tui::{
    backend::Backend,
    layout::{Alignment, Rect},
    style::{Color, Modifier, Style},
    text::Span,
    widgets::{Block, BorderType, Borders},
    Frame,
};

pub struct MainContext {
    pub new_proj_popup: bool,
    debug_popup: bool,
}

impl MainContext {
    pub fn new() -> MainContext {
        MainContext {
            new_proj_popup: false,
            debug_popup: false,
        }
    }

    pub fn debug<'a>(&'a mut self, dbg: bool) -> &'a mut MainContext {
        self.debug_popup = dbg;
        self
    }

    pub fn create(&self) -> MainContext {
        MainContext {
            new_proj_popup: false,
            debug_popup: self.debug_popup,
        }
    }
}

pub fn draw<B: Backend>(f: &mut Frame<B>, app: &mut AppInstance, area: Rect) {
    let block = Block::default()
        .borders(Borders::ALL)
        .title(Span::styled(
            "TaskManager",
            Style::default()
                .bg(Color::Magenta)
                .add_modifier(Modifier::BOLD),
        ))
        .title_alignment(Alignment::Center)
        .border_type(BorderType::Rounded);

    f.render_widget(block, area);

    // Draw master list
    common::draw_list(f, app, area, "Task List");
    if app.main_context.new_proj_popup == true {
        common::draw_popup(f, 60, 20);
    }
    if app.main_context.debug_popup == true {
        common::draw_debug_popup(f, app, area);
    }
}

pub fn register_key_event(key: KeyEvent, app: &mut AppInstance) {
    match key.code {
        KeyCode::Char('q') => app.set_current_context_type(ContextType::Quit),
        KeyCode::Char('n') => {
            app.main_context().new_proj_popup = !app.main_context().new_proj_popup
        }
        KeyCode::Char('?') => {
            app.set_current_context_type(ContextType::HelpPage);
        }
        KeyCode::Left => app.master_list.unselect(),
        KeyCode::Down => app.master_list.next(),
        KeyCode::Up => app.master_list.previous(),
        _ => {}
    }
}
