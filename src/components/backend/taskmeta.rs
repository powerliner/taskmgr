struct TaskMeta {
    meta: Vec<String>,
}

impl std::fmt::Display for TaskMeta {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for field in &self.meta {
            writeln!(f, "{}", field)?;
        }
        Ok(())
    }
}
