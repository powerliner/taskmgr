use crate::components::backend::task::Task;
use crate::components::backend::tasklist::TaskList;
use crate::components::ui::elements::{help_page, main_page};

pub struct AppInstance {
    pub master_list: TaskList,
    pub current_context: ContextType,
    pub main_context: main_page::MainContext,
    pub help_context: help_page::HelpContext,
}

pub enum ContextType {
    MainPage,
    HelpPage,
    Quit,
}

impl AppInstance {
    pub fn new() -> AppInstance {
        AppInstance {
            master_list: TaskList::gulp(Vec::new()),
            current_context: ContextType::MainPage,
            main_context: main_page::MainContext::new(),
            help_context: help_page::HelpContext::new(),
        }
    }

    #[cold]
    pub fn new_populated_app() -> AppInstance {
        let mut tasklist = TaskList::gulp(Vec::new());
        tasklist
            .list()
            .push(Task::default("Hello World".to_string()));
        tasklist.list().push(Task::default("Hi there".to_string()));
        tasklist.list().push(Task::default("12345".to_string()));
        AppInstance {
            master_list: tasklist,
            current_context: ContextType::MainPage,
            main_context: main_page::MainContext::new().debug(true).create(),
            help_context: help_page::HelpContext::new().debug(true).create(),
        }
    }

    pub fn get_current_context_type(&self) -> &ContextType {
        &self.current_context
    }

    pub fn set_current_context_type(&mut self, new_context: ContextType) {
        self.current_context = new_context;
    }

    pub fn main_context(&mut self) -> &mut main_page::MainContext {
        &mut self.main_context
    }

    pub fn help_context(&mut self) -> &mut help_page::HelpContext {
        &mut self.help_context
    }
}

impl std::fmt::Display for ContextType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let stringy: String;
        match self {
            ContextType::MainPage => stringy = String::from("MainPage"),
            ContextType::HelpPage => stringy = String::from("HelpPage"),
            ContextType::Quit => stringy = String::from("Quit"),
        }
        write!(f, "{}", stringy)
    }
}
