pub struct Task {
    pub name: String,
    pub priority: TaskPriority,
    pub catagory: TaskCatagory,
}

#[derive(Copy, Clone)]
pub enum TaskPriority {
    High,
    Medium,
    Low,
}
#[derive(Copy, Clone)]
pub enum TaskCatagory {
    Finished,
    Unfinished,
}

impl Task {
    // boilerplate get & set
    pub fn default(incoming_name: String) -> Task {
        Task {
            name: incoming_name,
            priority: TaskPriority::Medium,
            catagory: TaskCatagory::Unfinished,
        }
    }

    pub fn get_name(&mut self) -> String {
        self.name.to_string()
    }
    pub fn get_priority(&mut self) -> TaskPriority {
        self.priority
    }
    pub fn get_catagory(&self) -> TaskCatagory {
        self.catagory
    }

    pub fn set_name(&mut self, new_name: &mut str) {
        self.name = String::from(new_name);
    }
    pub fn set_priority(&mut self, new_priority: TaskPriority) {
        self.priority = new_priority;
    }
    pub fn set_catagory(&mut self, new_catagory: TaskCatagory) {
        self.catagory = new_catagory;
    }
}

impl std::fmt::Display for TaskCatagory {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let stringy: String;
        match self {
            TaskCatagory::Finished => stringy = String::from("Finished"),
            TaskCatagory::Unfinished => stringy = String::from("Unfinished"),
        }
        write!(f, "{}", stringy)
    }
}

impl std::fmt::Display for TaskPriority {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let stringy: String;
        match self {
            TaskPriority::High => stringy = String::from("High"),
            TaskPriority::Medium => stringy = String::from("Medium"),
            TaskPriority::Low => stringy = String::from("Low"),
        }
        write!(f, "{}", stringy)
    }
}

impl std::fmt::Display for Task {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}, {}, {}", self.name, self.priority, self.catagory)
    }
}
