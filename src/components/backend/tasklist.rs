use crate::components::backend::task::Task;
use tui::widgets::ListState;

pub struct TaskList {
    task_list: Vec<Task>,
    pub state: ListState,
}

impl TaskList {
    pub fn gulp(incoming_list: Vec<Task>) -> TaskList {
        TaskList {
            task_list: incoming_list,
            state: ListState::default(),
        }
    }

    pub fn list(&mut self) -> &mut Vec<Task> {
        &mut self.task_list
    }

    pub fn next(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i >= self.task_list.len() - i {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    pub fn previous(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i == 0 {
                    self.task_list.len() - i - 1
                } else {
                    i - 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }
    pub fn unselect(&mut self) {
        self.state.select(None);
    }
}
