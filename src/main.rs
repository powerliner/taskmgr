use crate::components::backend::appinstance::AppInstance;
use crate::components::backend::task;
use crate::components::backend::tasklist;
pub mod components;

fn main() {
    let app = AppInstance::new_populated_app();
    components::ui::terminalui::setup(app).unwrap();
}
